package com.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.demo.base.entity.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author 莯子
 * @since 2021-12-20
 */
@Data
@TableName("member")
public class Member extends BaseEntity {

    private static final long serialVersionUID = -3306120382564873486L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 账号
     */
    @TableField("`account`")
    private String account;

    /**
     * 密码,默认00000000
     */
    @TableField("`password`")
    private String password;

    /**
     * 头像图片地址
     */
    @TableField("avatar")
    private String avatar;

    /**
     * 电话号码
     */
    @TableField("phonenum")
    private String phonenum;

    /**
     * 姓名
     */
    @TableField("`name`")
    private String name;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 性别 0:男 1:女 2:其他
     */
    @TableField("sex")
    private Integer sex;

    /**
     * 年龄
     */
    @TableField("age")
    private Integer age;

    /**
     * 生日
     */
    @TableField("birthday")
    private String birthday;

    /**
     * 级别
     */
    @TableField("`level`")
    private String level;

    /**
     * 邮箱
     */
    @TableField("email")
    private String email;

    /**
     * 权限 0-99,默认00000000
     */
    @TableField("`limit`")
    private String limit;

    /**
     * 密码错误次数,3次锁定
     */
    @TableField("passerrnum")
    private int passerrnum;

    /**
     * 租户ID
     */
    @TableField("tenantid")
    private String tenantid;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

    /**
     * 创建时间
     */
    @TableField("createtime")
    private Date createtime;

    /**
     * 更新时间
     */
    @TableField("updatetime")
    private Date updatetime;


}
