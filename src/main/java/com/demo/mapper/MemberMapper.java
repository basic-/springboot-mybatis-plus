package com.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.demo.entity.Member;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 莯子
 * @since 2021-12-20
 */
@Mapper
public interface MemberMapper extends BaseMapper<Member> {

}
