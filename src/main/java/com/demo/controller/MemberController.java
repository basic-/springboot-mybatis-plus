package com.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.demo.base.controller.BaseController;
import com.demo.entity.Member;
import com.demo.mapper.MemberMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 */
@RestController
@RequestMapping("/member")
public class MemberController extends BaseController {

//    /**
//     * Field注入MemberMapper
//     */
//    @Autowired
//    private MemberMapper memberMapper;

    // Spring不推荐Field注入MemberMapper，可以使用以下构造注入方式
    /**
     * MemberMapper
     */
    private final MemberMapper memberMapper;

    /**
     * 构造注入
     *
     * @param memberMapper
     */
    public MemberController(MemberMapper memberMapper) {
        this.memberMapper = memberMapper;
    }


    /**
     * 查询所有
     *
     * @return 所有Member列表
     */
    @GetMapping("lists")
    public List<Member> selectLists() {
        return memberMapper.selectList(null);
    }

    /**
     * 条件查询
     *
     * @return 符合条件的Member列表
     */
    @GetMapping("list")
    public List<Member> selectList() {
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("address", "蒙德").ne("sex", "1");
        return memberMapper.selectList(queryWrapper);
    }


    @GetMapping("hello")
    public String hello() {
        return "Hello";
    }
}

