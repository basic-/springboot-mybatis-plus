package com.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.demo.entity.Member;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 莯子
 * @since 2021-12-20
 */
public interface MemberService extends IService<Member> {

}
