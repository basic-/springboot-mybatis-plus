/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MariaDB
 Source Server Version : 100605
 Source Host           : 8.140.190.51:3306
 Source Schema         : polar

 Target Server Type    : MariaDB
 Target Server Version : 100605
 File Encoding         : 65001

 Date: 20/12/2021 14:46:13
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '账号',
  `password` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL DEFAULT '00000000' COMMENT '密码,默认00000000',
  `avatar` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '头像图片地址',
  `phonenum` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '电话号码',
  `name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `address` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '地址',
  `sex` int(1) NULL DEFAULT NULL COMMENT '性别 0:男 1:女 2:其他',
  `age` int(3) NULL DEFAULT NULL COMMENT '年龄',
  `birthday` varchar(8) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '生日',
  `level` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '级别',
  `email` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '邮箱',
  `limit` varchar(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT '99' COMMENT '权限 0-99,默认00000000',
  `passerrnum` tinyint(1) NULL DEFAULT NULL COMMENT '密码错误次数,3次锁定',
  `tenantid` varchar(36) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '租户ID',
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '备注',
  `createtime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `updatetime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES (1, 'diluke', '00000000', '', '13100010001', '迪卢克', '蒙德', 0, 3, '20200101', '0', 'diluke@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (2, 'shatang', '00000000', '', '13100010002', '砂糖', '蒙德', 1, 10, '20200101', '0', 'shatang@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (3, 'ningguang', '00000000', '', '13100010003', '凝光', '蒙德', 1, 10, '20200101', '0', 'ningguang@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (4, 'kaiya', '00000000', '', '13100010004', '凯亚', '蒙德', 0, 10, '20200101', '0', 'kaiya@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (5, 'zaoyou', '00000000', '', '13100010005', '早柚', '蒙德', 1, 10, '20200101', '0', 'zaoyou@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (6, 'keqing', '00000000', '', '13100010006', '刻晴', '蒙德', 1, 10, '20200101', '0', 'keqing@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (7, 'keli', '00000000', '', '13100010007', '可莉', '蒙德', 1, 10, '20200101', '0', 'keli@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (8, 'ying', '00000000', '', '13100010008', '荧', '异世界', 1, 10, '20200101', '0', 'ying@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (9, 'xinyan', '00000000', '', '13100010009', '辛焱', '蒙德', 1, 10, '20200101', '0', 'xinyan@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (10, 'xiaogong', '00000000', '', '13100010010', '宵宫', '蒙德', 1, 10, '20200101', '0', 'xiaogong@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (11, 'hutao', '00000000', '', '13100010011', '胡桃', '蒙德', 1, 10, '20200101', '0', 'hutao@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (12, 'wendi', '00000000', '', '13100010012', '温迪', '蒙德', 0, 10, '20200101', '0', 'wendi@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (13, 'jiutiaosuoluo', '00000000', '', '13100010013', '九条娑罗', '稻妻', 1, 10, '20200101', '0', 'jiutiaosuoluo@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (14, 'shenlilinghua', '00000000', '', '13100010014', '神里绫华', '稻妻', 1, 10, '20200101', '0', 'shenlilinghua@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (15, 'qin', '00000000', '', '13100010015', '琴', '蒙德', 1, 10, '20200101', '0', 'qin@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (16, 'chongyun', '00000000', '', '13100010016', '重云', '蒙德', 0, 10, '20200101', '0', 'chongyun@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (17, 'nuoaier', '00000000', '', '13100010017', '诺艾尔', '蒙德', 1, 10, '20200101', '0', 'nuoaier@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (18, 'feixieer', '00000000', '', '13100010018', '菲谢尔', '蒙德', 1, 10, '20200101', '0', 'feixieer@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (19, 'xiao', '00000000', '', '13100010019', '魈', '蒙德', 0, 10, '20200101', '0', 'xiao@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (20, 'leize', '00000000', '', '13100010020', '雷泽', '蒙德', 0, 10, '20200101', '0', 'leize@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (21, 'anbo', '00000000', '', '13100010021', '安柏', '蒙德', 1, 10, '20200101', '0', 'anbo@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (22, 'babala', '00000000', '', '13100010022', '芭芭拉', '蒙德', 1, 10, '20200101', '0', 'babala@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (23, 'baer', '00000000', '', '13100010023', '巴尔', '蒙德', 1, 10, '20200101', '0', 'baer@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (24, 'qiqi', '00000000', '', '13100010024', '七七', '蒙德', 1, 10, '20200101', '0', 'qiqi@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (25, 'mona', '00000000', '', '13100010025', '莫娜', '蒙德', 1, 10, '20200101', '0', 'mona@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (26, 'lisha', '00000000', '', '13100010026', '丽莎', '蒙德', 1, 10, '20200101', '0', 'lisha@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (27, 'beidou', '00000000', '', '13100010027', '北斗', '蒙德', 1, 10, '20200101', '0', 'beidou@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (28, 'bannite', '00000000', '', '13100010028', '班尼特', '蒙德', 0, 10, '20200101', '0', 'bannite@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (29, 'xiangling', '00000000', '', '13100010029', '香菱', '蒙德', 1, 10, '20200101', '0', 'xiangling@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (30, 'xingqiu', '00000000', '', '13100010030', '行秋', '蒙德', 0, 10, '20200101', '0', 'xingqiu@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (31, 'diaona', '00000000', '', '13100010031', '迪奥娜', '蒙德', 1, 10, '20200101', '0', 'diaona@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (32, 'dadaliya', '00000000', '', '13100010032', '达达利亚', '蒙德', 0, 10, '20200101', '0', 'dadaliya@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (33, 'abeiduo', '00000000', '', '13100010033', '阿贝多', '蒙德', 0, 10, '20200101', '0', 'abeiduo@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (34, 'ganyu', '00000000', '', '13100010034', '甘雨', '蒙德', 1, 10, '20200101', '0', 'ganyu@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (35, 'zhongli', '00000000', '', '13100010035', '钟离', '蒙德', 0, 10, '20200101', '0', 'zhongli@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (36, 'luoshaliya', '00000000', '', '13100010036', '罗莎莉亚', '蒙德', 1, 10, '20200101', '0', 'luoshaliya@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (37, 'yanfei', '00000000', '', '13100010037', '烟绯', '蒙德', 1, 10, '20200101', '0', 'yanfei@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (38, 'youla', '00000000', '', '13100010038', '优菈', '蒙德', 1, 10, '20200101', '0', 'youla@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (39, 'fengyuanwanye', '00000000', '', '13100010039', '枫原万叶', '稻妻', 0, 10, '20200101', '0', 'fengyuanwanye@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (40, 'ailuoyi', '00000000', '', '13100010040', '埃洛伊', '异世界', 1, 10, '20200101', '0', 'ailuoyi@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (41, 'kong', '00000000', '', '13100010041', '空', '异世界', 0, 10, '20200101', '0', 'kong@ys.com', '99', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');
INSERT INTO `member` VALUES (43, 'muzi', '00000000', '', '13100010041', '莯子', '神界', 0, 10, '20200101', '0', 'muzi@ys.com', '9', 0, '', '', '2021-08-24 19:41:14', '2021-08-24 19:41:19');

SET FOREIGN_KEY_CHECKS = 1;
